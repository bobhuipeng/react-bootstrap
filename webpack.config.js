const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
//import CopyWebpackPlugin from 'copy-webpack-plugin'


module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: './index',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js',
    publicPath: '/assets/'
  },
  devServer: {
    inline: true,
    contentBase: './dist',
    port: 9000
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ["latest", 'es2015', 'react', 'stage-0']
          }
        }

      },
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'react', 'stage-0']
          }
        }

      },
      {
        test: /\.scss$/,
        exclude: /(node_modules)/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]

      },
      {
        test: /\.css$/,
        exclude: /(node_modules)/,
        use: [
          'style-loader', 
          'css-loader'
        ]

      },
      {
        test: /\.png$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 100000,
            mimetype: 'image/png'
          }
        }]

      },
      {
        test: /\.jpg$/,
        exclude: /(node_modules)/,
        use: ['file-loader']
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }]
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/octet-stream'
          }
        }]
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(node_modules)/,
        use: ['file']
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml'
          }
        }]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: 'public/*.html',
      to: 'dist/',
      toType: 'dir'
    }])
  ],
  resolve: {

    extensions: ['.js', '.jsx']
  }
}