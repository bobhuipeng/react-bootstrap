
 const hostUrl = 'http://localhost:8080'
 const analyzeUrl = `${hostUrl}/analyze`
 const suggestedIdUrl = `${hostUrl}/customer/suggestedIds`
 const customerIdLength = 9
const config ={
  hostUrl,
  analyzeUrl,
  suggestedIdUrl,
  customerIdLength,
}

export default config;