import React, { Component } from 'react';

import './App.css';

import SearchCustomer from './components/searchCustomer/SearchCustomer'

class App extends Component {
  render() {
    return (
      <div>
        <h1 className="App-title text-primary">Profiling Customers</h1>
        <SearchCustomer/>
      </div>
    );
  }
}

export default App;
