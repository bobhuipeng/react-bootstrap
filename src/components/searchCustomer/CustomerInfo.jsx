import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';

const CustomerInfo = ({ name, currentBalance, customerClassification }) => (
  <Container >
    <Row>
      <div className='yellow-line'>Customer Name: {name}</div>
    </Row>
    <Row>
      <div className='yellow-line'>Current Balance: {currentBalance}</div>
    </Row>
    <Row>
      <div className='yellow-line'>Customer Classification: {customerClassification}</div>
    </Row>
  </Container>)

export default CustomerInfo