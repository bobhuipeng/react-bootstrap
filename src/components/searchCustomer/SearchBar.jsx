import React, { Component } from 'react'
import { Button } from 'reactstrap';

import Autocomplete from 'react-autocomplete'
import axios from 'axios'
import MonthPickerInput from 'react-month-picker-input';

import { Container, Row, Col } from 'reactstrap';
import Modal from 'react-modal';

import '../../react-month-picker-input.css';

import config from '../../config'

var appElement = document.getElementById('root');
Modal.setAppElement(appElement)

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-90%, -90%)'
  }
};
const numExp = new RegExp(/^[\d]+$/)
export default class SearchInput extends Component {

  constructor(props) {
    super(props)
    this.today = new Date();
    this.state = {
      modalIsOpen: false,
      customerId: '',
      month: (this.today.getUTCMonth() + 1) + '-' + this.today.getUTCFullYear(),
      suggestedCustomerIds: []
    }
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  getSuggestedCustomer = (e) => {
    let inputValue = e.target.value
    this.message = this.checkCustomerId(inputValue)
    if (this.message !== '') {
      this.openModal()
    } else {
      if (inputValue !== undefined && inputValue.length > 3) {
        if (inputValue.length > config.customerIdLength) {
          inputValue = inputValue.substring(0, config.customerIdLength)
        }
        // make api call get suggested user list
        let url = `${config.suggestedIdUrl}/${inputValue}`
        axios.get(url).then(response => {
          let respCustIds = response.data;

          if (respCustIds.length > 0) {
            let suggestedCustomerIds = []
            respCustIds.forEach(id => {
              suggestedCustomerIds.push({ label: `${id}` })
            });

            this.setState({
              suggestedCustomerIds: suggestedCustomerIds
            })
          }

        })
      }
    }

    this.setState({ customerId: inputValue })
    //this.props.setSearchKeyWords(e.target.value);
  }

  searchCustomer = () => {
    this.message = this.checkCustomerId(this.state.customerId, true)

    if (this.message !== '') {
      this.openModal()
    } else {
      // call api to search customer , and update transactions
      this.props.searchCustomer(this.state.customerId, this.state.month)
    }
  }

  checkCustomerId = (customerId, checkLength = false) => {

    let message = ''
    if (customerId === '' || customerId === undefined) {
      message = 'Please input customer ID!'
    } else if (!numExp.test(customerId)) {
      message = 'Customer ID is Numbers ONLY!'
    }

    if (customerId.length !== config.customerIdLength && checkLength) {
      message = message + `ID lenght is NOT ${config.customerIdLength}!`
    }
    
    return message
  }

  autoCompleteRenderInput = (item, isHighlighted) =>
    <div key={item.label} style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
      {item.label}
    </div>

  render() {

    return (
      <Container>
        <Row>
          <Col>
            <div id='search-customer-id' className='input-group'>
              <div className='input-group-prepend'>
                <span className="input-group-text"> CustomerId:</span>
              </div>

              <Autocomplete
                getItemValue={(item) => item.label}
                items={this.state.suggestedCustomerIds}
                renderItem={this.autoCompleteRenderInput}
                value={this.state.customerId}
                onChange={this.getSuggestedCustomer}
                onSelect={(val) => this.setState({ customerId: val })}
                renderInput={props => <input {...props} className="form-control" placeholder="Customer ID" />}
              />
            </div>
          </Col>
          <Col>
            <div id='month-select-input-id' className='search-bar-date-picker-flex-container input-group' >
              <div className='input-group-prepend'> <span className="input-group-text">Date:</span></div>

              <MonthPickerInput
                value={this.state.month}
                year={this.today.getUTCFullYear()}
                month={this.today.getUTCMonth()}
                closeOnSelect={true}
                onChange={(selectedMonth, selectedYear) => {
                  this.setState({
                    month: String(selectedMonth).substring(0, 2) + "-" + selectedYear
                  })
                }}
                className="form-control"
              />
            </div>
          </Col>
          <Col>
            <Modal
              isOpen={this.state.modalIsOpen}
              onRequestClose={this.closeModal}
              style={customStyles}
              contentLabel="Query Warnning Message"
            >
              <div>
                {this.message}
              </div>
              <div className='center-display'>
                <Button outline color="warning" onClick={this.closeModal}>
                  Search
              </Button>
              </div>
            </Modal>
            <Button outline color="primary" onClick={this.searchCustomer}>
              Search
           </Button>
          </Col>
        </Row>
      </Container>
    )
  }
}
