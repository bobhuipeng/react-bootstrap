import React from 'react'
import PropTypes from 'prop-types';
import Moment from 'moment';
import { Table } from 'reactstrap';

const SingleTansaction = ({ localDateTime, message, type, amount, balance }) =>{
  Moment.locale('en');
 
  return   (<tr>
    <td>
      { Moment(localDateTime).format('ddd DD-MMM-YYYY HH:mm:ss')}
    </td>
    <td>
      {message}
    </td>
    <td>
      {type === 'debit' ? amount:''}
    </td>
    <td>
      {type === 'credit' ? amount:''}
    </td>
    <td>
      ${balance} CR
    </td>
  </tr>)
}


const Transactions = ({ transactions }) => (
  <Table striped bordered hover>
    <thead>
      <tr>
        <th>Date</th>
        <th>Transaction</th>
        <th>Debit</th>
        <th>Credit</th>
        <th>Balance</th>
      </tr>
    </thead>
    <tbody>
      {
        transactions.sort((a, b) => a.id < b.id).map((trans) => <SingleTansaction key={trans.id} {...trans} />)
      }
    </tbody>
  </Table >
)

Transactions.propTypes = {
  transactions: PropTypes.array.isRequired
}

export default Transactions
