import React, { Component } from 'react'

import { Container, Row, Col } from 'reactstrap';

import axios from 'axios'

import SearchBar from './SearchBar'
import CustomerInfo from './CustomerInfo'
import Transactions from './Transactions'
import config from '../../config'

export default class SearchCustomer extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  constructor(props) {
    super(props)
    this.state = {
      customer: {
        currentBalance: 0,
        customerClassification: ''
      },
      transactions: []
    }
  }

  searchCustomer = (customerId, month) => {
    //make api call to search customer
    let url = `${config.analyzeUrl}/${customerId}/${month}`
    console.log(url)
    axios.get(url)
      .then((response) => {
        this.setState({
          customer: {
            currentBalance: response.data.currentBalance,
            customerClassification: response.data.classification.join(),
            name:response.data.name,
          },
          transactions: response.data.transactions
        })
      })
      .catch((error) => {
        console.log(error);
      });

  }


  render() {
    return (
      <Container>
        <Row>
          <SearchBar searchCustomer={this.searchCustomer} />
        </Row>
        <Row className='top-row'>
          <CustomerInfo {...this.state.customer} />
        </Row>
        <Row className='top-row'>
          <Transactions {...this.state} />
        </Row>
      </Container>
    )
  }
}
